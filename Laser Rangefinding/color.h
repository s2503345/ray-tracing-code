#ifndef COLOR_H
#define COLOR_H

#include "vec3.h"
#include <iostream>
#include <cmath>

using color = vec3;

inline double linear_to_gamma(double linear_component)
{
    return sqrt(linear_component);
}

void write_color(std::ostream &out, double pixel_distance, int samples_per_pixel) {

    // Divide the color by the number of samples.
    auto scale = 1.0 / samples_per_pixel;
    pixel_distance *= scale;
//    if (pixel_distance > 1600)
//	    pixel_distance -= 1600;
//
//    auto white = pixel_distance;
//    white = 16*log(white + 1)/log(2);
//    if (white > 255.999)
//	white = 255.999;

//    out << static_cast<int>(white) << ' '
//        << static_cast<int>(white) << ' '
//        << static_cast<int>(white) << '\n';

    if (pixel_distance <= 1600)
	    out << static_cast<int>(0) << ' '
        	<< static_cast<int>(0) << ' '
        	<< static_cast<int>(0) << '\n';
    if (pixel_distance > 1600 && pixel_distance <= 1800)
            out << static_cast<int>(2.55*(pixel_distance - 1600)/2) << ' '
                << static_cast<int>(0) << ' '
                << static_cast<int>(0) << '\n';
    if (pixel_distance > 1800 && pixel_distance <= 2000)
            out << static_cast<int>(255) << ' '
                << static_cast<int>(2.55*(pixel_distance - 1800)/2) << ' '
                << static_cast<int>(0) << '\n';
    if (pixel_distance > 2000 && pixel_distance <= 2400)
            out << static_cast<int>(255 - 2.55*(pixel_distance - 2000)/4) << ' '
                << static_cast<int>(255) << ' '
                << static_cast<int>(0) << '\n';
    if(pixel_distance > 2400 && pixel_distance <= 3200)
            out << static_cast<int>(0) << ' '
                << static_cast<int>(255) << ' '
                << static_cast<int>(2.55*(pixel_distance - 2400)/8) << '\n';
    if (pixel_distance > 3200 && pixel_distance <= 4800)
            out << static_cast<int>(0) << ' '
                << static_cast<int>(255 - 2.55*(pixel_distance - 3200)/16) << ' '
                << static_cast<int>(255) << '\n';
    if (pixel_distance > 4800 && pixel_distance <= 8000)
            out << static_cast<int>(2.55*(pixel_distance - 4800)/32) << ' '
                << static_cast<int>(0) << ' '
                << static_cast<int>(255) << '\n';
    if (pixel_distance > 8000 && pixel_distance <= 14400)
            out << static_cast<int>(255) << ' '
                << static_cast<int>(2.55*(pixel_distance - 8000)/64) << ' '
                << static_cast<int>(255) << '\n';
    if (pixel_distance > 14400)
            out << static_cast<int>(255) << ' '
                << static_cast<int>(255) << ' '
                << static_cast<int>(255) << '\n';


}

#endif
